package it.jxhem.wunderly;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import it.jxhem.wunderly.carslist.view.PlacemarksListFragmentTest;
import it.jxhem.wunderly.carsmap.view.PlacemarksMapFragmentTest;
import it.jxhem.wunderly.main.view.activity.MainActivityTest;

@RunWith(Suite.class)
@Suite.SuiteClasses(
  {
    MainActivityTest.class,
    PlacemarksListFragmentTest.class,
    PlacemarksMapFragmentTest.class
  })
public class TestSuite {
}
