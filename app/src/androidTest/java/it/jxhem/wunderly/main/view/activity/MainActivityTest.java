package it.jxhem.wunderly.main.view.activity;

import android.content.Context;
import android.support.test.annotation.UiThreadTest;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.gson.Gson;
import com.karumi.rosie.daggerutils.ForApplication;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import it.jxhem.wunderly.InjectedInsturmentationTest;
import it.jxhem.wunderly.base.repository.datasource.PlaceMarksFakeDataSource;
import it.jxhem.wunderly.base.repository.datasource.PlacemarksApiDataSource;
import it.jxhem.wunderly.base.repository.datasource.PlacemarksRealmDataSource;
import it.jxhem.wunderly.carslist.view.fragment.PlacemarksListFragment;
import it.jxhem.wunderly.carsmap.view.fragment.PlacemarksMapFragment;
import okhttp3.OkHttpClient;

import static junit.framework.Assert.assertNotNull;


@RunWith(AndroidJUnit4.class)
public class MainActivityTest extends
  InjectedInsturmentationTest {


  @Rule
  public IntentsTestRule<MainActivity> activityRule =
    new IntentsTestRule<>(MainActivity.class, true, true);

  @Test
  @UiThreadTest
  public void checkComponents() {
    assertNotNull(activityRule.getActivity());
  }


  @Override
  public List<Object> getTestModules() {
    return Arrays.asList((Object) new TestModule());
  }

  @Module(
    overrides = true,
    library = true,
    complete = false,
    injects = {
      MainActivity.class,
      PlacemarksListFragment.class,
      PlacemarksMapFragment.class,
      MainActivityTest.class
    })
  class TestModule {

    private final OkHttpClient okhttpClient = new OkHttpClient();
    private Gson gson = new Gson();

    @Provides
    @Singleton
    public PlacemarksApiDataSource providePlacemarksApiDataSource() {
      return new PlacemarksApiDataSource(okhttpClient, gson);
    }

    @Provides
    @Singleton
    public PlaceMarksFakeDataSource provideFakeDataSource() {
      return new PlaceMarksFakeDataSource();
    }


    @Provides
    @Singleton
    public PlacemarksRealmDataSource provideRealmDataSource(
      @ForApplication Context context
    ) {
      return new PlacemarksRealmDataSource(context);
    }

    @Provides
    public boolean provideFakeDataSourceEnabled() {
      return true;
    }
  }
}
