package it.jxhem.wunderly.recyclerview;

import android.content.res.Resources;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.PerformException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.espresso.util.HumanReadables;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class RecyclerItemViewAssertion<A> implements ViewAssertion {

  private int position;
  private A item;
  private RecyclerViewInteraction.ItemViewAssertion<A> itemViewAssertion;

  public RecyclerItemViewAssertion(int position, A item,
                                   RecyclerViewInteraction
                                       .ItemViewAssertion<A>
                                       itemViewAssertion) {
    this.position = position;
    this.item = item;
    this.itemViewAssertion = itemViewAssertion;
  }

  @Override
  public final void check(View view, NoMatchingViewException e) {
    RecyclerView recyclerView = (RecyclerView) view;
    RecyclerView.ViewHolder viewHolderForPosition =
        recyclerView.findViewHolderForLayoutPosition(position);
    if (viewHolderForPosition == null) {
      throw (new PerformException.Builder()).withActionDescription(toString())
          .withViewDescription(HumanReadables.describe(view))
          .withCause(new IllegalStateException("No view holder at position: "
              + position))
          .build();
    } else {
      View viewAtPosition = viewHolderForPosition.itemView;
      itemViewAssertion.check(item, viewAtPosition, e);
    }
  }


  //from https://stackoverflow.com/questions/26150522/espresso-withtext-in-textview-doesnt-match-selected-view
  //customized to correctily consider text
  public static Matcher<View> withText(final String resource) {

    return new BoundedMatcher<View, TextView>(TextView.class) {
      private String resourceName = null;
      private String expectedText = null;

      @Override
      public void describeTo(Description description) {
        description.appendText("with string from resource id: ");
        description.appendText(resource);
        if (null != this.resourceName) {
          description.appendText("[");
          description.appendText(this.resourceName);
          description.appendText("]");
        }
        if (null != this.expectedText) {
          description.appendText(" value: ");
          description.appendText(this.expectedText);
        }
      }

      @Override
      public boolean matchesSafely(TextView textView) {
        if (null == this.expectedText) {
          try {
            this.expectedText = textView.getText().toString();
          } catch (Resources.NotFoundException ignored) {
                    /*
                     * view could be from a context unaware of the resource
                     * id.
                     */
          }
        }
        if (null != this.expectedText) {
          return this.expectedText.equals(textView.getText()
            .toString());
        } else {
          return false;
        }
      }
    };
  }
}