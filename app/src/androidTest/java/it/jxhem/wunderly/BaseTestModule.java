package it.jxhem.wunderly;


import android.content.Context;

import com.karumi.rosie.daggerutils.ForActivity;

import dagger.Module;
import dagger.Provides;
import it.jxhem.wunderly.main.MainModule;

@Module(overrides = true, complete = false, library = true, includes = {
    MainModule.class
})
public class BaseTestModule {

  private final Context context;

  public BaseTestModule(Context context) {
    this.context = context;
  }

  @Provides
  @ForActivity
  Context provideActivityContext() {
    return context;
  }
}