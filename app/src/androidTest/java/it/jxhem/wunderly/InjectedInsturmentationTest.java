package it.jxhem.wunderly;

import android.app.Instrumentation;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.IdlingResource;

import org.junit.After;
import org.junit.Before;

import java.util.LinkedList;
import java.util.List;

import dagger.ObjectGraph;
import it.jxhem.wunderly.main.WunderlyApplication;

import static android.support.test.espresso.Espresso.getIdlingResources;
import static android.support.test.espresso.Espresso.unregisterIdlingResources;


/***
 * Copied from the Rosie/sample example
 */
public abstract class InjectedInsturmentationTest {
  @Before
  public void setUp() {
    WunderlyApplication application = getApplication();
    List<Object> childTestModules = getTestModules();
    Context context = InstrumentationRegistry.getInstrumentation()
        .getTargetContext();
    List<Object> testModules = new LinkedList<>(childTestModules);
    testModules.add(new BaseTestModule(context));
    ObjectGraph objectGraph = application.plusGraph(testModules);
    application.replaceGraph(objectGraph);
    objectGraph.inject(this);
  }

  @After
  public void tearDown() throws Exception {
    List<IdlingResource> idlingResources = getIdlingResources();
    for (IdlingResource resource : idlingResources) {
      unregisterIdlingResources(resource);
    }
    WunderlyApplication application = getApplication();
    application.resetFakeGraph();
  }

  private WunderlyApplication getApplication() {
    Instrumentation instrumentation = InstrumentationRegistry
        .getInstrumentation();
    WunderlyApplication app =
        (WunderlyApplication) instrumentation.getTargetContext()
            .getApplicationContext();
    return app;
  }

  public abstract List<Object> getTestModules();
}
