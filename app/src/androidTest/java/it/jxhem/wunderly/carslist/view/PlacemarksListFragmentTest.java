package it.jxhem.wunderly.carslist.view;

import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;
import it.jxhem.wunderly.InjectedInsturmentationTest;
import it.jxhem.wunderly.R;
import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.repository.PlacemarkDataSourceFactory;
import it.jxhem.wunderly.base.repository.PlacemarksRepository;
import it.jxhem.wunderly.carslist.view.fragment.PlacemarksListFragment;
import it.jxhem.wunderly.main.view.activity.MainActivity;
import it.jxhem.wunderly.recyclerview.RecyclerItemViewAssertion;
import it.jxhem.wunderly.recyclerview.RecyclerViewInteraction;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class PlacemarksListFragmentTest extends InjectedInsturmentationTest {

  @Rule
  public IntentsTestRule<MainActivity> activityRule =
    new IntentsTestRule<>(MainActivity.class, true, true);

  @Inject
  PlacemarksRepository placemarksRepository;

  @Test
  public void shouldShowPlacemarks() throws Exception {
    List<Placemark> placemarks = givenPlacemarks();

    int position = (int) (Math.random() * placemarks.size());

    onView(withId(R.id.driversList))
      .perform(RecyclerViewActions
        .scrollToPosition(position));

    assertRecyclerViewShowsPlacemarks(placemarks);
  }

  private List<Placemark> givenPlacemarks() throws Exception {
    return (List<Placemark>) placemarksRepository.getAll();
  }

  private void assertRecyclerViewShowsPlacemarks(final List<Placemark>
                                                   placemarks) {
    RecyclerViewInteraction.<Placemark>onRecyclerView(

      withId(R.id.driversList))
      .withItems(placemarks)
      .check(new RecyclerViewInteraction.ItemViewAssertion<Placemark>() {
        @Override
        public void check(
          Placemark placemark,
          View view,
          NoMatchingViewException e
        ) {
          matches(hasDescendant(RecyclerItemViewAssertion.withText(placemark.getVin()))).check(view, e);
        }
      });
  }

  @Override
  public List<Object> getTestModules() {
    return Arrays.asList((Object) new TestModule());
  }

  @Module(
    library = true,
    complete = false,
    injects = {
      PlacemarkDataSourceFactory.class,
      PlacemarksListFragment.class,
      PlacemarksListFragmentTest.class
    })
  class TestModule {
    @Provides
    public boolean provideFakeDataSourceEnabled() {
      return true;
    }
  }
}
