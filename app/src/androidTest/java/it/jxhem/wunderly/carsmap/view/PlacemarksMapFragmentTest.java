package it.jxhem.wunderly.carsmap.view;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiSelector;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;
import it.jxhem.wunderly.InjectedInsturmentationTest;
import it.jxhem.wunderly.R;
import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.repository.PlacemarkDataSourceFactory;
import it.jxhem.wunderly.base.repository.PlacemarksRepository;
import it.jxhem.wunderly.carsmap.view.fragment.PlacemarksMapFragment;
import it.jxhem.wunderly.main.view.activity.MainActivity;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class PlacemarksMapFragmentTest extends InjectedInsturmentationTest {

  @Inject
  PlacemarksRepository placemarksRepository;

  @Rule
  public IntentsTestRule<MainActivity> activityRule =
    new IntentsTestRule<>(MainActivity.class, true, true);

  @Test
  public void performSwipe() {
    onView(withId(R.id.container))
      .check(matches(isDisplayed()));

    onView(withId(R.id.container))
      .perform(swipeLeft());

    assertThat(activityRule.getActivity().getPagerAdapter().getTitle(0), Matchers.equalTo("List"));
    assertThat(activityRule.getActivity().getPagerAdapter().getTitle(1), Matchers.equalTo("Map"));
  }
  
  @Test
  public void shouldShowMarkers() throws Exception {

    onView(withId(R.id.container))
      .check(matches(isDisplayed()));

    onView(withId(R.id.container))
      .perform(swipeLeft());

    Collection<Placemark> pmarks = givenPlacemarks();
    final ArrayList<Placemark> placemarks = new ArrayList<>();
    for (Placemark placemark : pmarks) {
      placemarks.add(placemark);
    }

    UiDevice device = UiDevice.getInstance(getInstrumentation());
    for (int i = 0; i < placemarks.size(); i++) {
      UiObject marker = device.findObject(new UiSelector().descriptionContains(placemarks.get(i).getName()));
      assertNotNull(marker.exists());
    }
  }

  @Test
  public void shouldShowOnlyMarker() throws Exception {
    onView(withId(R.id.container))
      .check(matches(isDisplayed()));

    onView(withId(R.id.container))
      .perform(swipeLeft());

    Collection<Placemark> pmarks = givenPlacemarks();
    final ArrayList<Placemark> placemarks = new ArrayList<>();
    for (Placemark placemark : pmarks) {
      placemarks.add(placemark);
    }

    UiDevice device = UiDevice.getInstance(getInstrumentation());
    UiObject marker = device.findObject(new UiSelector().descriptionContains(placemarks.get(4).getName()));
    UiObject marker_2 = device.findObject(new UiSelector().descriptionContains(placemarks.get(0).getName()));

    assertTrue(marker.exists());
    assertTrue(marker_2.exists());

    marker.clickAndWaitForNewWindow(3000);
    marker = device.findObject(new UiSelector().descriptionContains(placemarks.get(4).getName()));
    marker_2 = device.findObject(new UiSelector().descriptionContains(placemarks.get(0).getName()));

    assertTrue(marker.exists());
    assertFalse(marker_2.exists());
  }


  private Collection<Placemark> givenPlacemarks() throws Exception {
    return placemarksRepository.getAll();
  }

  @Override
  public List<Object> getTestModules() {
    return Arrays.asList((Object) new TestModule());
  }


  @Module(
    library = true,
    complete = false,
    injects = {
      PlacemarkDataSourceFactory.class,
      PlacemarksMapFragment.class,
      PlacemarksMapFragmentTest.class
    })
  class TestModule {
    @Provides
    public boolean provideFakeDataSourceEnabled() {
      return true;
    }
  }
}
