package it.jxhem.wunderly.carslist.view.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karumi.rosie.view.Presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import it.jxhem.wunderly.R;
import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.view.fragment.WunderlyFragment;
import it.jxhem.wunderly.carslist.view.presenter.PlacemarksListPresenter;
import it.jxhem.wunderly.databinding.FragmentListBinding;
import it.jxhem.wunderly.databinding.RvDriverItemBinding;

public class PlacemarksListFragment extends WunderlyFragment implements PlacemarksListPresenter.View {

  private RecyclerView mRV;
  private LayoutInflater mInflater;
  private FragmentListBinding mainBinding;

  @Inject
  @Presenter
  PlacemarksListPresenter presenter;

  private List<Placemark> mPlaceMarks;
  private PlacemarksAdapter adapter;

  @Override
  protected int getLayoutId() {
    return R.layout.fragment_list;
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mInflater = getLayoutInflater(null);
    adapter = new PlacemarksAdapter(mInflater);
    mainBinding = FragmentListBinding.bind(view);
    mRV = mainBinding.driversList;
    showLoading();
  }

  @Override
  public void hideLoading() {

  }

  @Override
  public void showLoading() {

  }

  @Override
  public void showPlacemarks(ArrayList<Placemark> placemarks) {
    mPlaceMarks = placemarks;
    mRV.post(new Runnable() {
      @Override
      public void run() {
        mRV.setLayoutManager(new LinearLayoutManager(getContext()));
        mRV.setAdapter(adapter);
        adapter.notifyDataSetChanged();
      }
    });
    hideLoading();
  }

  private class PlacemarksAdapter extends RecyclerView.Adapter<DriverViewHolder> {

    private LayoutInflater inflater;

    public PlacemarksAdapter(LayoutInflater inflater) {
      this.inflater = inflater;
    }

    @Override
    public DriverViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      RvDriverItemBinding binding = RvDriverItemBinding.inflate
        (inflater, parent, false);
      return new DriverViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(DriverViewHolder holder, int position) {

      RvDriverItemBinding binding =
        DataBindingUtil.getBinding(holder.itemView);

      binding.setPlacemark(mPlaceMarks.get(position));
    }

    @Override
    public int getItemCount() {
      return mPlaceMarks.size();
    }
  }

  private class DriverViewHolder extends RecyclerView.ViewHolder {

    public DriverViewHolder(View itemView) {
      super(itemView);
    }
  }

}
