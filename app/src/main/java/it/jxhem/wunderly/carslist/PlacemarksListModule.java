package it.jxhem.wunderly.carslist;

import dagger.Module;
import it.jxhem.wunderly.carslist.view.fragment.PlacemarksListFragment;

@Module(
  library = true,
  complete = false,
  injects = {
    PlacemarksListFragment.class
  })
public class PlacemarksListModule {
}