package it.jxhem.wunderly.carslist.view.presenter;

import com.karumi.rosie.domain.usecase.UseCaseHandler;
import com.karumi.rosie.domain.usecase.annotation.Success;
import com.karumi.rosie.domain.usecase.callback.OnSuccessCallback;
import com.karumi.rosie.domain.usecase.error.OnErrorCallback;

import java.util.ArrayList;

import javax.inject.Inject;

import it.jxhem.wunderly.base.domain.usecase.GetPlacemarksList;
import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.view.error.ConnectionError;
import it.jxhem.wunderly.base.view.presenter.WunderlyPresenter;

public class PlacemarksListPresenter extends WunderlyPresenter<PlacemarksListPresenter.View> {

  private final OnSuccessCallback successCallback;
  private final OnErrorCallback errorCallback;

  @Inject
  public PlacemarksListPresenter(
    UseCaseHandler useCaseHandler,
    GetPlacemarksList getPlacemarksList
  ) {
    super(useCaseHandler, getPlacemarksList);

    errorCallback = new OnErrorCallback() {
      public boolean onError(Error error) {
        if(error instanceof ConnectionError)
          getView().showConnectionError();
        else
          getView().showGenericError();
        return true;
      }
    };

    successCallback = new OnSuccessCallback() {
      @Success
      public void onPlacemarksListLoaded(ArrayList<Placemark> placemarks) {
        loadPlaceMarks(placemarks);
      }
    };
  }

  private void loadPlaceMarks(ArrayList<Placemark> placemarks) {
    getView().showPlacemarks(placemarks);
  }

  @Override
  public void update() {
    super.update();
    getPlaceMarks(successCallback, errorCallback);
  }

  public interface View extends WunderlyPresenter.View {
    void showPlacemarks(ArrayList<Placemark> placemarks);
  }
}
