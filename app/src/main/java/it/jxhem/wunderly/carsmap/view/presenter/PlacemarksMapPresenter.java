package it.jxhem.wunderly.carsmap.view.presenter;

import com.karumi.rosie.domain.usecase.UseCaseHandler;
import com.karumi.rosie.domain.usecase.annotation.Success;
import com.karumi.rosie.domain.usecase.callback.OnSuccessCallback;
import com.karumi.rosie.domain.usecase.error.OnErrorCallback;

import java.util.ArrayList;

import javax.inject.Inject;

import it.jxhem.wunderly.base.domain.usecase.GetPlacemarksList;
import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.view.error.ConnectionError;
import it.jxhem.wunderly.base.view.presenter.WunderlyPresenter;
import it.jxhem.wunderly.carsmap.domain.usecase.InteractWithMap;


public class PlacemarksMapPresenter extends WunderlyPresenter<PlacemarksMapPresenter.View> {

  private final OnErrorCallback errorCallback;
  private final OnSuccessCallback successCallback;
  private final InteractWithMap interactWithMap;


  @Inject
  public PlacemarksMapPresenter(
    UseCaseHandler useCaseHandler,
    GetPlacemarksList getPlacemarksList,
    InteractWithMap interactWithMap
  ) {
    super(useCaseHandler, getPlacemarksList);
    this.interactWithMap = interactWithMap;

    errorCallback = new OnErrorCallback() {
      public boolean onError(Error error) {
        if(error instanceof ConnectionError)
          getView().showConnectionError();
        else
          getView().showGenericError();
        return true;
      }
    };

    successCallback = new OnSuccessCallback() {
      @Success
      public void onPlacemarksListLoaded(ArrayList<Placemark> placemarks) {
        //If size is 0 it means we have no data in our DB
        loadPlaceMarks(placemarks);
      }
    };
  }

  @Override
  public void update() {
    super.update();
    super.getPlaceMarks(successCallback, errorCallback);
  }

  public void mapReady() {
    createUseCaseCall(interactWithMap)
      .useCaseName(InteractWithMap.SHOW_MARKERS)
      .onSuccess(new OnSuccessCallback() {
        @Success
        public void showPlacemarks(ArrayList<Placemark> placemarks) {
          loadPlaceMarks(placemarks);
        }
      })
      .onError(errorCallback)
      .execute();
  }

  private void loadPlaceMarks(ArrayList<Placemark> placemarks) {
    getView().showMarkers(placemarks);
  }

  public void markerSelected(String placemarkId) {
    createUseCaseCall(interactWithMap)
      .args(placemarkId)
      .useCaseName(InteractWithMap.SHOW_MARKER)
      .onSuccess(new OnSuccessCallback() {
        @Success
        public void showMarker(Placemark placemark) {
          getView().showMarker(placemark);
        }
      })
      .onError(errorCallback)
      .execute();
  }

  public interface View extends WunderlyPresenter.View {
    void showMarker(Placemark placemark);

    void showMarkers(ArrayList<Placemark> placemarks);
  }
}
