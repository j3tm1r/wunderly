package it.jxhem.wunderly.carsmap;

import dagger.Module;
import it.jxhem.wunderly.carsmap.view.fragment.PlacemarksMapFragment;

@Module(
  library = true,
  complete = false,
  injects = {
    PlacemarksMapFragment.class
  })
public class PlacemarksMapModule {
}
