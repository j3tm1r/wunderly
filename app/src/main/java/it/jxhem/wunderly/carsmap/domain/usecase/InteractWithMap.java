package it.jxhem.wunderly.carsmap.domain.usecase;

import com.karumi.rosie.domain.usecase.RosieUseCase;
import com.karumi.rosie.domain.usecase.annotation.UseCase;

import java.util.Collection;

import javax.inject.Inject;

import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.repository.PlacemarksRepository;

public class InteractWithMap extends RosieUseCase {
  public static final String SHOW_MARKERS = "showMarkers";
  public static final String SHOW_MARKER = "showMarker";
  private final PlacemarksRepository placeMarksRepository;

  @Inject
  public InteractWithMap(PlacemarksRepository placeMarksRepository) {

    this.placeMarksRepository = placeMarksRepository;
  }


  @UseCase(name = SHOW_MARKER)
  public void showMarker(String id) throws Exception {
    Placemark placemark = placeMarksRepository.getByKey(id);
    notifySuccess(placemark);
  }

  @UseCase(name = SHOW_MARKERS)
  public void showMarkers() throws Exception {
    Collection<Placemark> placemarks = placeMarksRepository.getAll();
    notifySuccess(placemarks);
  }
}
