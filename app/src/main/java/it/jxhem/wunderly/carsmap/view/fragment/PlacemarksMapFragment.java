package it.jxhem.wunderly.carsmap.view.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.karumi.rosie.view.Presenter;

import java.util.ArrayList;

import javax.inject.Inject;

import it.jxhem.wunderly.R;
import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.view.fragment.WunderlyFragment;
import it.jxhem.wunderly.carsmap.view.presenter.PlacemarksMapPresenter;

public class PlacemarksMapFragment extends WunderlyFragment
  implements PlacemarksMapPresenter.View, OnMapReadyCallback {

  private static final float ZOOM_LEVEL = 13;
  private SupportMapFragment mapFragment;
  private GoogleMap mMap;
  private BitmapDescriptor mBitmapDescriptor;
  private GoogleMap.OnMarkerClickListener showMarkersListener;
  private GoogleMap.OnMarkerClickListener showMarkerListener;

  @Override
  protected int getLayoutId() {
    return R.layout.fragment_map;
  }

  @Inject
  @Presenter
  PlacemarksMapPresenter placemarksMapPresenter;

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mapFragment = new SupportMapFragment();
    getFragmentManager()
      .beginTransaction()
      .add(R.id.map_fragment, mapFragment)
      .commit();
    mapFragment.getMapAsync(this);


    showMarkersListener = new GoogleMap.OnMarkerClickListener() {
      @Override
      public boolean onMarkerClick(Marker marker) {
        placemarksMapPresenter.mapReady();
        return true;
      }
    };

    showMarkerListener = new GoogleMap.OnMarkerClickListener() {
      @Override
      public boolean onMarkerClick(Marker marker) {
        placemarksMapPresenter.markerSelected(marker.getSnippet());
        return true;
      }
    };
  }

  @Override
  public void hideLoading() {

  }

  @Override
  public void showLoading() {

  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    mBitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.pin_car_green_40);
    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
      ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 0);
    } else
      mMap.setMyLocationEnabled(true);
    mMap.setOnMarkerClickListener(showMarkerListener);
    mMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM_LEVEL));
    placemarksMapPresenter.mapReady();
  }

  //TODO change type of placemark to Presenter Model
  @Override
  public void showMarker(Placemark placemark) {
    mMap.clear();
    mMap.setOnMarkerClickListener(null);
    LatLng location = new LatLng(
      placemark.getCoordinates().get(0),
      placemark.getCoordinates().get(1)
    );
    Marker new_marker = mMap.addMarker(
      new MarkerOptions()
        .position(location)
        .snippet(placemark.getVin())
        .title(placemark.getName())
        .icon(mBitmapDescriptor)
    );
    new_marker.showInfoWindow();
    mMap.setOnMarkerClickListener(showMarkersListener);
    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, ZOOM_LEVEL));
  }

  @Override
  public void showMarkers(ArrayList<Placemark> placemarks) {
    if (mMap == null)
      return;

    LatLng location = null;
    mMap.clear();
    for (Placemark placemark : placemarks) {
      location = new LatLng(
        placemark.getCoordinates().get(0),
        placemark.getCoordinates().get(1)
      );
      mMap.addMarker(
        new MarkerOptions()
          .position(location)
          .snippet(placemark.getVin())
          .title(placemark.getName())
          .icon(mBitmapDescriptor)
      );
    }
    if (location != null)
      mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, ZOOM_LEVEL));
    mMap.setOnMarkerClickListener(showMarkerListener);
  }
}
