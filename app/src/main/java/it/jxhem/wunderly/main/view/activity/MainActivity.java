package it.jxhem.wunderly.main.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.karumi.rosie.view.Presenter;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import it.jxhem.wunderly.R;
import it.jxhem.wunderly.base.view.acitvity.WunderlyAcitvity;
import it.jxhem.wunderly.carslist.view.fragment.PlacemarksListFragment;
import it.jxhem.wunderly.carsmap.view.fragment.PlacemarksMapFragment;
import it.jxhem.wunderly.databinding.ActivityMainBinding;
import it.jxhem.wunderly.main.MainModule;
import it.jxhem.wunderly.main.fragment.FragmentAdapter;
import it.jxhem.wunderly.main.presenter.MainActivityPresenter;

public class MainActivity extends WunderlyAcitvity implements MainActivityPresenter.View {

  private FragmentAdapter mPagerAdapter;
  private ViewPager mViewPager;

  private ActivityMainBinding mainBinding;

  @Inject
  @Presenter
  MainActivityPresenter mainActivityPresenter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    mainBinding = DataBindingUtil.setContentView(this, R.layout
      .activity_main);
    initializeViewPager();
  }

  private void initializeViewPager() {
    mPagerAdapter = new FragmentAdapter(getSupportFragmentManager());

    // Set up the ViewPager with the sections adapter.
    mViewPager = mainBinding.container;
    mViewPager.setAdapter(mPagerAdapter);

    TabLayout tabLayout = mainBinding.tabs;
    tabLayout.setupWithViewPager(mViewPager);
  }

  @Override
  protected int getLayoutId() {
    return R.layout.activity_main;
  }

  @Override
  protected List<Object> getActivityScopeModules() {
    return Arrays.asList((Object) new MainModule());
  }

  @Override
  public void hideLoading() {

  }

  @Override
  public void showLoading() {

  }

  @Override
  public void loadData() {
    PlacemarksListFragment placemarksListFragment = new PlacemarksListFragment();
    PlacemarksMapFragment placemarksMapFragment = new PlacemarksMapFragment();

    mPagerAdapter.addFragment(placemarksListFragment, "List");
    mPagerAdapter.addFragment(placemarksMapFragment, "Map");
    mPagerAdapter.notifyDataSetChanged();
  }

  public FragmentAdapter getPagerAdapter(){
    return mPagerAdapter;
  }
}
