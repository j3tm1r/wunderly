package it.jxhem.wunderly.main;

import dagger.Module;
import it.jxhem.wunderly.carslist.view.fragment.PlacemarksListFragment;
import it.jxhem.wunderly.carsmap.view.fragment.PlacemarksMapFragment;
import it.jxhem.wunderly.main.view.activity.MainActivity;

@Module(
  library = true,
  complete = false,
  injects = {
    MainActivity.class,
    PlacemarksListFragment.class,
    PlacemarksMapFragment.class
  })
public class MainModule {
}