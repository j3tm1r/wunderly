package it.jxhem.wunderly.main.fragment;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class FragmentAdapter extends FragmentPagerAdapter {

  private final List<ViewPagerFragmentHolder> fragments = new ArrayList<>();
  private final List<String> titles = new ArrayList<>();

  public FragmentAdapter(FragmentManager fm) {
    super(fm);
  }

  @Override
  public Fragment getItem(int position) {
    ViewPagerFragmentHolder fragmentHolder = fragments.get(position);
    return fragmentHolder.fragment;
  }

  @Override
  public int getCount() {
    // Show 3 total pages.
    return fragments.size();
  }

  @Override
  public CharSequence getPageTitle(int position) {
    ViewPagerFragmentHolder fragmentHolder = fragments.get(position);
    return fragmentHolder.pageTitle;
  }

  public void addFragment(Fragment fragment, String pageTitle) {
    fragments.add(new ViewPagerFragmentHolder(fragment, pageTitle));
    titles.add(pageTitle);
  }

  public String getTitle(int position) {
    return titles.get(position);
  }

  private static class ViewPagerFragmentHolder {
    public final Fragment fragment;
    public final String pageTitle;

    public ViewPagerFragmentHolder(Fragment fragment, String
      pageTitle) {
      this.fragment = fragment;
      this.pageTitle = pageTitle;
    }
  }
}