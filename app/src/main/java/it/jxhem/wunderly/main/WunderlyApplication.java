package it.jxhem.wunderly.main;

import com.karumi.rosie.application.RosieApplication;

import java.util.Arrays;
import java.util.List;

import dagger.ObjectGraph;

public class WunderlyApplication extends RosieApplication {
  private ObjectGraph objectGraph;

  @Override
  protected List<Object> getApplicationModules() {
    return Arrays.asList((Object) new AppModule(this));
  }

  public void replaceGraph(ObjectGraph objectGraph) {
    this.objectGraph = objectGraph;
  }

  @Override
  public ObjectGraph plusGraph(List<Object> activityScopeModules) {
    ObjectGraph newObjectGraph;
    if (objectGraph == null) {
      newObjectGraph = super.plusGraph(activityScopeModules);
    } else {
      newObjectGraph = objectGraph.plus(activityScopeModules.toArray());
    }
    return newObjectGraph;
  }

  public void resetFakeGraph() {
    objectGraph = null;
  }
}
