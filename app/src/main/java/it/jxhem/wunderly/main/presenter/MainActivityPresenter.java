package it.jxhem.wunderly.main.presenter;

import com.karumi.rosie.domain.usecase.UseCaseHandler;
import com.karumi.rosie.domain.usecase.annotation.Success;
import com.karumi.rosie.domain.usecase.callback.OnSuccessCallback;
import com.karumi.rosie.domain.usecase.error.OnErrorCallback;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import it.jxhem.wunderly.base.domain.usecase.DownloadplaceMarks;
import it.jxhem.wunderly.base.domain.usecase.GetPlacemarksList;
import it.jxhem.wunderly.base.domain.usecase.PersistPlacemarks;
import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.view.error.ConnectionError;
import it.jxhem.wunderly.base.view.presenter.WunderlyPresenter;

public class MainActivityPresenter extends WunderlyPresenter<MainActivityPresenter.View> {


  private final DownloadplaceMarks downloadplaceMarks;
  private final PersistPlacemarks persistPlacemarks;
  private final OnSuccessCallback successDownloadCallbak;
  private final OnSuccessCallback successPersistCallback;
  private final OnErrorCallback errorCallback;
  private final OnSuccessCallback successCallback;

  @Inject
  public MainActivityPresenter(
    UseCaseHandler useCaseHandler,
    DownloadplaceMarks downloadplaceMarks,
    PersistPlacemarks persistPlacemarks,
    GetPlacemarksList getPlacemarksList
  ) {
    super(useCaseHandler, getPlacemarksList);
    this.downloadplaceMarks = downloadplaceMarks;
    this.persistPlacemarks = persistPlacemarks;

    errorCallback = new OnErrorCallback() {
      @Override
      public boolean onError(Error error) {
        if(error instanceof ConnectionError)
          getView().showConnectionError();
        else
          getView().showGenericError();
        return true;
      }
    };

    successPersistCallback = new OnSuccessCallback() {
      @Success
      public void onPlacemarksPersisted() {
        getView().loadData();
      }
    };

    successDownloadCallbak = new OnSuccessCallback() {
      @Success
      public void onPlacemarksDownloaded(Collection<Placemark> placemarks) {
        persistPlacemarks(
          successPersistCallback,
          errorCallback,
          placemarks
        );
      }
    };

    successCallback = new OnSuccessCallback() {
      @Success
      public void onPlacemarksListLoaded(ArrayList<Placemark> placemarks) {
        if (placemarks.size() == 0) {
          downloadPlaceMarks(
            successDownloadCallbak,
            errorCallback
          );
        } else
          getView().loadData();
      }
    };
  }

  @Override
  public void initialize() {
    super.initialize();
    getPlaceMarks(successCallback, errorCallback);
  }

  protected void downloadPlaceMarks(
    OnSuccessCallback successCallback,
    OnErrorCallback errorCallback
  ) {
    createUseCaseCall(downloadplaceMarks)
      .onSuccess(successCallback)
      .onError(errorCallback)
      .execute();
  }

  protected void persistPlacemarks(
    OnSuccessCallback successCallback,
    OnErrorCallback errorCallback,
    Collection<Placemark> placemarks
  ) {
    createUseCaseCall(persistPlacemarks)
      .args(placemarks)
      .onSuccess(successCallback)
      .onError(errorCallback)
      .execute();
  }

  public interface View extends WunderlyPresenter.View {
    public void loadData();
  }
}
