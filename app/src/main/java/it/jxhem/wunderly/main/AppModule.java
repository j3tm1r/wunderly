package it.jxhem.wunderly.main;

import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import it.jxhem.wunderly.base.repository.datasource.PlaceMarksFakeDataSource;
import it.jxhem.wunderly.base.repository.datasource.PlacemarksApiDataSource;
import it.jxhem.wunderly.base.repository.datasource.PlacemarksRealmDataSource;
import okhttp3.OkHttpClient;

@Module(
  library = true,
  complete = false,
  injects = {
    WunderlyApplication.class
  })
public class AppModule {
  private final Context context;
  private final OkHttpClient okhttpClient = new OkHttpClient();
  private Gson gson = new Gson();

  public AppModule(Context context) {
    this.context = context;
  }

  @Provides
  @Singleton
  public PlacemarksApiDataSource providePlacemarksApiDataSource() {
    return new PlacemarksApiDataSource(okhttpClient, gson);
  }

  @Provides
  @Singleton
  public PlaceMarksFakeDataSource provideFakeDataSource() {
    return new PlaceMarksFakeDataSource();
  }


  @Provides
  @Singleton
  public PlacemarksRealmDataSource provideRealmDataSource() {
    return new PlacemarksRealmDataSource(context);
  }

  @Provides
  public boolean provideFakeDataSourceEnabled() {
    return false;
  }
}
