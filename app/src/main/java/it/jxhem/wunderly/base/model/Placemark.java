package it.jxhem.wunderly.base.model;

import com.karumi.rosie.repository.datasource.Identifiable;

import java.util.ArrayList;

public class Placemark implements Identifiable<String> {

  String address;
  String engineType;
  String exterior;
  String interior;
  String name;
  String vin;
  int fuel;
  ArrayList<Double> coordinates;
  private transient int id;

  private Placemark(Builder builder) {
    this.address = builder.address;
    this.name = builder.name;
    this.engineType = builder.engineType;
    this.exterior = builder.exterior;
    this.interior = builder.interior;
    this.vin = builder.vin;
    this.fuel = builder.fuel;
    this.coordinates = new ArrayList<>();
    this.coordinates.add(builder.latitude);
    this.coordinates.add(builder.longitude);
    this.coordinates.add(0d);
    this.id = builder.key;
  }

  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  public String getEngineType() {
    return engineType;
  }

  public String getExterior() {
    return exterior;
  }

  public String getInterior() {
    return interior;
  }

  public String getVin() {
    return vin;
  }

  public int getFuel() {
    return fuel;
  }

  public ArrayList<Double> getCoordinates() {
    return coordinates;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public void setEngineType(String engineType) {
    this.engineType = engineType;
  }

  public void setExterior(String exterior) {
    this.exterior = exterior;
  }

  public void setInterior(String interior) {
    this.interior = interior;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setVin(String vin) {
    this.vin = vin;
  }

  public void setFuel(int fuel) {
    this.fuel = fuel;
  }

  public void setCoordinates(ArrayList<Double> coordinates) {
    this.coordinates = coordinates;
  }

  @Override
  public String getKey() {
    return vin;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public static class Builder {
    String address, engineType, exterior, interior, name, vin;
    int fuel, key;
    double latitude, longitude;

    public Builder setAddress(String address) {
      this.address = address;
      return this;
    }

    public Builder setEngineType(String engineType) {
      this.engineType = engineType;
      return this;
    }

    public Builder setExterior(String exterior) {
      this.exterior = exterior;
      return this;
    }

    public Builder setInterior(String interior) {
      this.interior = interior;
      return this;
    }

    public Builder setName(String name) {
      this.name = name;
      return this;
    }

    public Builder setVin(String vin) {
      this.vin = vin;
      return this;
    }

    public Builder setFuel(int fuel) {
      this.fuel = fuel;
      return this;
    }

    public Builder setLatitude(double latitude) {
      this.latitude = latitude;
      return this;
    }

    public Builder setLongitude(double longitude) {
      this.longitude = longitude;
      return this;
    }

    public Builder setKey(int key) {
      this.key = key;
      return this;
    }

    public Placemark build() {
      return new Placemark(this);
    }
  }
}
