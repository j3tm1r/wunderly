package it.jxhem.wunderly.base.view.fragment;

import android.support.design.widget.Snackbar;

import com.karumi.rosie.view.RosieSupportFragment;

import it.jxhem.wunderly.R;

public abstract class WunderlyFragment extends RosieSupportFragment {

  public void showGenericError() {
    Snackbar.make(getView(), getString(R.string.generic_error), Snackbar.LENGTH_SHORT).show();
  }

  public void showConnectionError() {
    Snackbar.make(getView(), getString(R.string.connection_error), Snackbar.LENGTH_SHORT).show();
  }
}
