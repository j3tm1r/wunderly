package it.jxhem.wunderly.base.repository.datasource;

import android.content.Context;

import com.karumi.rosie.daggerutils.ForApplication;
import com.karumi.rosie.repository.datasource.ReadableDataSource;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.model.PlacemarkRealm;
import it.jxhem.wunderly.base.repository.datasource.mapper.PlacemarkToPlacemarkRealmMapper;

public class PlacemarksRealmDataSource implements ReadableDataSource<String, Placemark> {

  private final PlacemarkToPlacemarkRealmMapper mapper;
  private final Context mContext;

  @Inject
  public PlacemarksRealmDataSource(@ForApplication Context appContext) {
    mContext = appContext;
    mapper = new PlacemarkToPlacemarkRealmMapper();
    initRealm();
  }

  private void initRealm() {
    Realm.init(mContext);
  }

  public void persistPlaceMarks(Collection<Placemark> placemarks) {
    Realm realm = Realm.getInstance(new RealmConfiguration.Builder().build());
    realm.beginTransaction();
    int key = 0;
    for (Placemark placemark : placemarks) {
      PlacemarkRealm placemarkRealm = mapper.map(placemark);
      placemarkRealm.setKey(key++);
      realm.copyToRealm(placemarkRealm);
    }
    realm.commitTransaction();
  }

  @Override
  public Placemark getByKey(String key) {
    Realm realm = Realm.getInstance(new RealmConfiguration.Builder().build());
    RealmResults<PlacemarkRealm> results =
      realm
        .where(PlacemarkRealm.class)
        .equalTo("vin", key)
        .findAll();
    return mapper.reverseMap(results.first());
  }

  @Override
  public Collection<Placemark> getAll() {
    Realm realm = Realm.getInstance(new RealmConfiguration.Builder().build());
    RealmResults<PlacemarkRealm> results =
      realm
        .where(PlacemarkRealm.class)
        .findAll();

    ArrayList<Placemark> placemarks = new ArrayList<>();

    for (PlacemarkRealm result : results) {
      placemarks.add(mapper.reverseMap(result));
    }
    return placemarks;
  }
}
