package it.jxhem.wunderly.base.domain.usecase;

import com.karumi.rosie.domain.usecase.RosieUseCase;
import com.karumi.rosie.domain.usecase.annotation.UseCase;

import java.util.ArrayList;

import javax.inject.Inject;

import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.repository.datasource.PlacemarksRealmDataSource;
import it.jxhem.wunderly.base.view.error.UnknownErorr;

public class PersistPlacemarks extends RosieUseCase {

  private final PlacemarksRealmDataSource realmDataSource;

  @Inject
  public PersistPlacemarks(PlacemarksRealmDataSource realmDataSource) {
    this.realmDataSource = realmDataSource;
  }

  @UseCase
  public void persistPlacemarks(ArrayList<Placemark> placemarks) {
    try {
      realmDataSource.persistPlaceMarks(placemarks);
      notifySuccess();
    } catch (Exception e) {
      throw new UnknownErorr();
    }
  }
}
