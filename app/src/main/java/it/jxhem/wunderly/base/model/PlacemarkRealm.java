package it.jxhem.wunderly.base.model;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class PlacemarkRealm extends RealmObject {
  private String address, engineType, exterior, interior, name;

  private int fuel;

  @PrimaryKey
  private int key;

  @Index
  private String vin;

  private double latitude, longitude;

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  public String getAddress() {
    return address;
  }

  public String getEngineType() {
    return engineType;
  }

  public String getExterior() {
    return exterior;
  }

  public String getInterior() {
    return interior;
  }

  public String getName() {
    return name;
  }

  public String getVin() {
    return vin;
  }

  public int getFuel() {
    return fuel;
  }

  public PlacemarkRealm() {
  }

  private PlacemarkRealm(PlacemarkRealm.Builder builder) {
    this.address = builder.address;
    this.name = builder.name;
    this.engineType = builder.engineType;
    this.exterior = builder.exterior;
    this.interior = builder.interior;
    this.vin = builder.vin;
    this.fuel = builder.fuel;
    this.latitude = builder.latitude;
    this.longitude = builder.longitude;
    this.key = builder.key;
  }

  public int getKey() {
    return key;
  }

  public void setKey(int key) {
    this.key = key;
  }

  public static class Builder {
    String address, engineType, exterior, interior, name, vin;
    int fuel, key;
    double latitude, longitude;

    public Builder setAddress(String address) {
      this.address = address;
      return this;
    }

    public Builder setEngineType(String engineType) {
      this.engineType = engineType;
      return this;
    }

    public Builder setExterior(String exterior) {
      this.exterior = exterior;
      return this;
    }

    public Builder setInterior(String interior) {
      this.interior = interior;
      return this;
    }

    public Builder setName(String name) {
      this.name = name;
      return this;
    }

    public Builder setVin(String vin) {
      this.vin = vin;
      return this;
    }

    public Builder setFuel(int fuel) {
      this.fuel = fuel;
      return this;
    }

    public Builder setLatitude(double latitude) {
      this.latitude = latitude;
      return this;
    }

    public Builder setLongitude(double longitude) {
      this.longitude = longitude;
      return this;
    }

    public Builder setKey(int key) {
      this.key = key;
      return this;
    }

    public PlacemarkRealm build() {
      return new PlacemarkRealm(this);
    }
  }
}
