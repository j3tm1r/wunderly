package it.jxhem.wunderly.base.repository.datasource;

import com.google.gson.Gson;
import com.karumi.rosie.repository.datasource.ReadableDataSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.inject.Inject;

import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.view.error.ConnectionError;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class PlacemarksApiDataSource implements ReadableDataSource<String, Placemark> {

  private Gson mGson;
  private OkHttpClient mClient;
  private HashMap<String, Placemark> mPlacemarks;

  @Inject
  public PlacemarksApiDataSource(OkHttpClient okHttpClient, Gson gson) {
    this.mGson = gson;
    this.mClient = okHttpClient;

    mPlacemarks = new HashMap<>();
  }

  public Placemark getByKey(String placemarkKey) {
    return mPlacemarks.get(placemarkKey);
  }

  @Override
  public Collection<Placemark> getAll() throws Exception {
    Request request = new Request.Builder()
      .url("https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json")
      .build();

    try {
      Response response = mClient.newCall(request).execute();
      MainObject mainObject = mGson.fromJson(response.body().charStream(), MainObject.class);
      for (Placemark placemark : mainObject.getPlacemarks()) {
        mPlacemarks.put(placemark.getKey(), placemark);
      }
      return mainObject.getPlacemarks();
    } catch (IOException e) {
      throw new ConnectionError();
    }
  }

  private class MainObject {
    ArrayList<Placemark> placemarks;

    public ArrayList<it.jxhem.wunderly.base.model.Placemark> getPlacemarks() {
      return placemarks;
    }
  }
}
