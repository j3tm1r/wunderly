package it.jxhem.wunderly.base.repository.datasource.mapper;

import com.karumi.rosie.mapper.Mapper;

import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.model.PlacemarkRealm;

public class PlacemarkToPlacemarkRealmMapper extends Mapper<Placemark, PlacemarkRealm> {
  @Override
  public PlacemarkRealm map(Placemark value) {
    PlacemarkRealm.Builder placemarkBuilder = new PlacemarkRealm.Builder();
    placemarkBuilder
      .setVin(value.getVin())
      .setAddress(value.getAddress())
      .setName(value.getName())
      .setFuel(value.getFuel())
      .setEngineType(value.getEngineType())
      .setExterior(value.getExterior())
      .setInterior(value.getInterior())
      .setLatitude(value.getCoordinates().get(1))
      .setLongitude(value.getCoordinates().get(0))
      .setKey(value.getId());

    return placemarkBuilder.build();
  }

  @Override
  public Placemark reverseMap(PlacemarkRealm value) {
    Placemark.Builder placemarkBuilder = new Placemark.Builder();
    placemarkBuilder
      .setVin(value.getVin())
      .setAddress(value.getAddress())
      .setName(value.getName())
      .setFuel(value.getFuel())
      .setEngineType(value.getEngineType())
      .setExterior(value.getExterior())
      .setInterior(value.getInterior())
      .setLatitude(value.getLatitude())
      .setLongitude(value.getLongitude())
      .setKey(value.getKey());

    return placemarkBuilder.build();
  }
}
