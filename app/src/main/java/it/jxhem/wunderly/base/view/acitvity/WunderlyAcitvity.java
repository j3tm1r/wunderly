package it.jxhem.wunderly.base.view.acitvity;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.karumi.rosie.view.RosieActivity;

import it.jxhem.wunderly.R;

public abstract class WunderlyAcitvity extends RosieActivity {


  public void showGenericError() {
    View rootView = getWindow().getDecorView().getRootView();
    Snackbar.make(rootView, getString(R.string.generic_error), Snackbar.LENGTH_SHORT).show();
  }

  public void showConnectionError() {
    View rootView = getWindow().getDecorView().getRootView();
    Snackbar.make(rootView, getString(R.string.connection_error), Snackbar.LENGTH_SHORT).show();
  }
}
