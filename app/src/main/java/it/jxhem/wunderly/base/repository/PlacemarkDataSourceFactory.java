package it.jxhem.wunderly.base.repository;

import com.karumi.rosie.repository.datasource.ReadableDataSource;

import javax.inject.Inject;

import it.jxhem.wunderly.BuildConfig;
import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.repository.datasource.PlaceMarksFakeDataSource;
import it.jxhem.wunderly.base.repository.datasource.PlacemarksRealmDataSource;

public class PlacemarkDataSourceFactory {

  @Inject
  PlaceMarksFakeDataSource fakeDataSource;

  @Inject
  PlacemarksRealmDataSource realmDataSource;

  @Inject
  boolean fakeDataSourceEnabled;

  @Inject
  PlacemarkDataSourceFactory() {
  }

  ReadableDataSource<String, Placemark> createDataSource() {
    if (BuildConfig.DEBUG && fakeDataSourceEnabled) {
      return fakeDataSource;
    }

    return realmDataSource;
  }
}
