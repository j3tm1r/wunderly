package it.jxhem.wunderly.base.domain.usecase;

import com.karumi.rosie.domain.usecase.RosieUseCase;
import com.karumi.rosie.domain.usecase.annotation.UseCase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.repository.PlacemarksRepository;
import it.jxhem.wunderly.base.view.error.ConnectionError;

public class GetPlacemarksList extends RosieUseCase {

  private final PlacemarksRepository placeMarksRepository;

  @Inject
  public GetPlacemarksList(PlacemarksRepository placeMarksRepository) {
    this.placeMarksRepository = placeMarksRepository;
  }

  @UseCase
  public void getPlacemarksList() throws Exception {

    Collection<Placemark> all;
    try {
      all = placeMarksRepository.getAll();
    } catch (Exception e) {
      all = new ArrayList<>();
      notifyError(new ConnectionError());
    }
    notifySuccess(all);
  }
}
