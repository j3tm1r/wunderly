package it.jxhem.wunderly.base.domain.usecase;

import com.karumi.rosie.domain.usecase.RosieUseCase;
import com.karumi.rosie.domain.usecase.annotation.UseCase;

import java.util.Collection;

import javax.inject.Inject;

import it.jxhem.wunderly.base.model.Placemark;
import it.jxhem.wunderly.base.repository.datasource.PlacemarksApiDataSource;
import it.jxhem.wunderly.base.view.error.ConnectionError;

public class DownloadplaceMarks extends RosieUseCase {

  private final PlacemarksApiDataSource placemarksApiDataSource;

  @Inject
  public DownloadplaceMarks(PlacemarksApiDataSource placemarksApiDataSource) {
    this.placemarksApiDataSource = placemarksApiDataSource;
  }

  @UseCase
  public void downloadPlaceMarks() {
    Collection<Placemark> placemarks;
    try {
      placemarks = placemarksApiDataSource.getAll();
    } catch (Exception e) {
      throw new ConnectionError();
    }
    notifySuccess(placemarks);
  }
}
