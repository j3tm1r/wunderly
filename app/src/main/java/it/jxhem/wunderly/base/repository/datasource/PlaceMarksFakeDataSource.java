package it.jxhem.wunderly.base.repository.datasource;

import com.karumi.rosie.repository.datasource.ReadableDataSource;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;

import it.jxhem.wunderly.base.model.Placemark;

public class PlaceMarksFakeDataSource implements
    ReadableDataSource<String, Placemark> {

  private ArrayList<Placemark> placemarks;

  @Inject
  public PlaceMarksFakeDataSource() {
    initPlacemarks();
  }

  private void initPlacemarks() {
    placemarks = new ArrayList<>();
    placemarks.add(getFirst());
    placemarks.add(getSecond());
    placemarks.add(getThird());
    placemarks.add(getFourth());
    placemarks.add(getFifth());
  }

  public Placemark getFirst() {
    Placemark.Builder placemarkB = new Placemark.Builder();
    placemarkB.setAddress("Lesserstraße 170, 22049 Hamburg");
    placemarkB.setLatitude(53.59301d);
    placemarkB.setLongitude(10.08526d);
    placemarkB.setEngineType("CE");
    placemarkB.setExterior("UNACCEPTABLE");
    placemarkB.setInterior("UNACCEPTABLE");
    placemarkB.setFuel(42);
    placemarkB.setName("HH-GO8522");
    placemarkB.setVin("WME4513341K565439");
    placemarkB.setKey(0);
    return placemarkB.build();
  }

  public Placemark getSecond() {
    Placemark.Builder placemarkB = new Placemark.Builder();
    placemarkB.setAddress("Grosse Reichenstraße 7, 20457 Hamburg");
    placemarkB.setLatitude(53.54847d);
    placemarkB.setLongitude(10.09622d);
    placemarkB.setEngineType("CE");
    placemarkB.setExterior("UNACCEPTABLE");
    placemarkB.setInterior("GOOD");
    placemarkB.setFuel(45);
    placemarkB.setName("HH-GO8480");
    placemarkB.setVin("WME4513341K412697");
    placemarkB.setKey(1);
    return placemarkB.build();
  }

  public Placemark getThird() {
    Placemark.Builder placemarkB = new Placemark.Builder();
    placemarkB.setAddress("Ring 2, 22043 Hamburg");
    placemarkB.setLatitude(53.56388d);
    placemarkB.setLongitude(10.07838d);
    placemarkB.setEngineType("CE");
    placemarkB.setExterior("GOOD");
    placemarkB.setInterior("GOOD");
    placemarkB.setFuel(84);
    placemarkB.setName("HH-GO8002");
    placemarkB.setVin("WME4513341K412713");
    placemarkB.setKey(2);
    return placemarkB.build();
  }

  public Placemark getFourth() {
    Placemark.Builder placemarkB = new Placemark.Builder();
    placemarkB.setAddress("Kroogblöcke 32, 22119 Hamburg");
    placemarkB.setLatitude(53.55258d);
    placemarkB.setLatitude(10.09416d);
    placemarkB.setEngineType("CE");
    placemarkB.setExterior("GOOD");
    placemarkB.setInterior("UNACCEPTABLE");
    placemarkB.setFuel(57);
    placemarkB.setName("HH-GO8004");
    placemarkB.setVin("WME4513341K412727");
    placemarkB.setKey(3);
    return placemarkB.build();
  }

  public Placemark getFifth() {
    Placemark.Builder placemarkB = new Placemark.Builder();
    placemarkB.setAddress("Schopenstehl 30, 20095 Hamburg");
    placemarkB.setLongitude(10.08815d);
    placemarkB.setLatitude(53.54867d);
    placemarkB.setEngineType("CE");
    placemarkB.setExterior("GOOD");
    placemarkB.setInterior("UNACCEPTABLE");
    placemarkB.setFuel(27);
    placemarkB.setName("HH-GO8079");
    placemarkB.setVin("WME4513341K413315");
    placemarkB.setKey(4);
    return placemarkB.build();
  }

  @Override
  public Placemark getByKey(String key) {
    for (Placemark placemark : placemarks)
      if (placemark.getKey().equals(key))
        return placemark;
    return null;
  }

  @Override
  public Collection<Placemark> getAll() {
    return placemarks;
  }
}
