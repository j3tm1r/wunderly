package it.jxhem.wunderly.base.repository;

import com.karumi.rosie.repository.RosieRepository;
import com.karumi.rosie.repository.datasource.ReadableDataSource;

import javax.inject.Inject;

import it.jxhem.wunderly.base.model.Placemark;

public class PlacemarksRepository extends RosieRepository<String, Placemark> {

  @Inject
  public PlacemarksRepository(
    PlacemarkDataSourceFactory dataSourceFactory) {

    ReadableDataSource<String, Placemark> placemarksDataSource =
      dataSourceFactory.createDataSource();

    addReadableDataSources(placemarksDataSource);
  }
}
