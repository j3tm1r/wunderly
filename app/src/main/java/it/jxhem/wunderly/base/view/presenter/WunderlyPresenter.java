package it.jxhem.wunderly.base.view.presenter;

import com.karumi.rosie.domain.usecase.UseCaseHandler;
import com.karumi.rosie.domain.usecase.callback.OnSuccessCallback;
import com.karumi.rosie.domain.usecase.error.OnErrorCallback;
import com.karumi.rosie.view.loading.RosiePresenterWithLoading;

import it.jxhem.wunderly.base.domain.usecase.GetPlacemarksList;
import it.jxhem.wunderly.base.view.error.ConnectionError;

public class WunderlyPresenter<T extends WunderlyPresenter.View> extends
  RosiePresenterWithLoading<T> {

  private final GetPlacemarksList getPlacemarksList;

  public WunderlyPresenter(UseCaseHandler useCaseHandler, GetPlacemarksList getPlacemarksList) {
    super(useCaseHandler);
    registerOnErrorCallback(onErrorCallback);
    this.getPlacemarksList = getPlacemarksList;
  }

  protected void getPlaceMarks(
    OnSuccessCallback successCallback, OnErrorCallback errorCallback
  ) {
    createUseCaseCall(getPlacemarksList)
      .onSuccess(successCallback)
      .onError(errorCallback)
      .execute();
  }

  private final OnErrorCallback onErrorCallback = new OnErrorCallback() {
    @Override
    public boolean onError(Error error) {
      if (error instanceof ConnectionError) {
        getView().showConnectionError();
      } else {
        getView().showGenericError();
      }
      return true;
    }
  };

  public interface View extends RosiePresenterWithLoading.View {
    void showGenericError();

    void showConnectionError();
  }
}
