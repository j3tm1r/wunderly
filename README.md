# Wunderly's Readme" 


Lately I have been reading about software architecture and came around
the Clean Architecture concept from Robert C. Martin. 
The main idea is to divide the application on many layers as represented by the following image.

The following five principles are the foundation of this system architecture (from  https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html). A Clean Architecture is :

* Independent of Frameworks. The architecture does not depend on the existence of some library of feature laden software. This allows you to use such frameworks as tools, rather than having to cram your system into their limited constraints.
* Testable. The business rules can be tested without the UI, Database, Web Server, or any other external element.
* Independent of UI. The UI can change easily, without changing the rest of the system. A Web UI could be replaced with a console UI, for example, without changing the business rules.
* Independent of Database. You can swap out Oracle or SQL Server, for Mongo, BigTable, CouchDB, or something else. Your business rules are not bound to the database.
* Independent of any external agency. In fact your business rules simply don’t know anything at all about the outside world. 

![cleanarch.jpg](https://bitbucket.org/repo/z8Xe56j/images/3821079236-cleanarch.jpg "Image from https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html")"

I did not implement the architecture layer by myself but used instead **Rosie** from **Karumi** ( **Github :** https://github.com/Karumi/Rosie ). The **sample** app on Rosie's Github repository was very inspiring on writing my application.

Apart from Rosie i used the usual Android libraries (RecyclerView, CardView, OkHttp, Gson, Play Services for Maps, Dagger and the native android-databinding instead of ButterKnife). To persist data I used Realm since I have been studying it lately.   

I wrote some instrumented tests in order to validate the  behavior of some functionalities of the app.

There is little to no documentation and I will provide it in the following days together with some unit tests since none was written.